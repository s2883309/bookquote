package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class QuoterTest {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> values = new HashMap<>();
        double final_var = 0;
        values.put("1", 10.0);
        values.put("2", 45.0);
        values.put("3", 20.0);
        values.put("4", 35.0);
        values.put("5", 50.0);

        if (values.containsKey(isbn)) {
            final_var = values.get(isbn);
        }
        return final_var;
    }
}
